# DPG_App

### Databáze

Jako databáze bylo použito lokálního uložiště - SQLite. Obsahuje 2 tabulky `Autor` a `Post`:

* `Autor` obsahuje informace o autorovi článku (id, jméno, příjmení). Jeden `Autor` může napsat M článků.
* `Post` obsahuje informace o článku (id, titulek, text, autorovoId). Jeden článek musí mít právě jednoho autora. 

### Rest Api

Vytvořeno pomocí .NET 5 a standartních knihoven. 
Použité NuGet balíčky: 

* Microsoft.EntityFrameworkCore.Sqlite
* Microsoft.EntityFrameworkCore.Tools

Databáze byla vygenerována z modelů (Author, Post) pomocí Entity Frameworku. Rest Api bylo testováno pomocí Postmanu (složka `RestApi/PostmanTest`) a Swaggeru.

Pro spuštění je potřeba .NET 5 runtime
Spuštění pomocí příkazu `dotnet run`, po spuštění se automaticky spustí stráky Swaggeru pro vyzkoušení API.

### Front End
Knihovny:
* Pyqt5
* requests, json
* (PyInstaller)

Aplikace má základní ochrany proti výpadkům na straně API, předpokládá se však, že bude funkční.
Filtrace nad daty je lokální na konzistentním modelu. 
Pomocí PyInstalleru pak byl vytvořen jednoduchý spouštěcí soubor .exe (`ezStart.rar`). Debugovací konzole je zapnutá.
