from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from controller import mainWindowController
class Application(QtWidgets.QApplication):
    def __init__(self, argv=None):
        super(Application, self).__init__(argv)
        self.windows = []
        win = mainWindowController.MainWindow(self)
        win.showMaximized()
        self.windows.append(win)
        win.installEventFilter(win)

if __name__ == "__main__":
    app = Application(sys.argv)
    app.setStyle("Fusion")
    sys.exit(app.exec_())