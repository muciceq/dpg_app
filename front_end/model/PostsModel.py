from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
import requests
import json
from PyQt5.QtCore import pyqtSignal, QObject
class PostsModel(QtCore.QAbstractTableModel):

    header_labels = ['Post ID', 'Author ID(FK)', 'Title' ,'Text']
    API_URL = "https://localhost:5001/api/Post/"
    connectionError = pyqtSignal(object)
    
    def __init__(self):
        super(PostsModel, self).__init__()

    def refreshData(self):
        try:
            request_data = requests.get(self.API_URL, verify=False)
            j_data = request_data.json()
            internal_list = []
            for item in j_data:
                i = [item["postId"], item["authorId"] ,item["title"], item["text"]]
                internal_list.append(i)
            self._data = internal_list
            self.layoutChanged.emit()
        except requests.exceptions.RequestException as error:
            self.connectionError.emit(error)


    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        if(len(self._data)!=0):
            return len(self._data[0])
        return len(self.header_labels)
    
    def flags(self, index):
        return Qt.ItemIsSelectable|Qt.ItemIsEnabled|Qt.ItemIsEditable

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self.header_labels[section]

    #edit
    def setData(self, index, value, role):
        if role == Qt.EditRole:
            updated_column = index.column() #index to update
            old_row = self._data[index.row()][:] #old row to update
            old_row[updated_column] = value
            req_json_body = {"postId":old_row[0],"authorId":old_row[1], "title": old_row[2], "text": old_row[3]}
            req_put = requests.put(self.API_URL+str(old_row[0]), json=req_json_body, verify=False)
            if req_put.ok:
                self._data[index.row()][index.column()] = value
                return True
            return False
    #delete
    def removeRows(self, row, rows=1, index=QtCore.QModelIndex()):
        if(row>=0):
            id_del = self._data[row][0] #id to delete
            req_del = requests.delete(self.API_URL+str(id_del), verify=False)
            if req_del.ok:
                self.beginRemoveRows(QtCore.QModelIndex(), row, row + rows - 1)
                self._data = self._data[:row] + self._data[row + rows:]
                self.endRemoveRows()
                return True
            return False
        return False
    #add
    def insertRows(self, row, data, count=1, index=QtCore.QModelIndex()):
        post_data = {"authorId":data[1],"title":data[2] ,"text": data[3]}
        post_req = requests.post(self.API_URL, json=post_data, verify=False)
        if(post_req.ok):
            response = post_req.json()
            _id = response['postId']
            data[0] = _id
            self.beginInsertRows(QtCore.QModelIndex(), row, row + count - 1)
            self._data.append(data)
            self.endInsertRows()
            return True
        return False
