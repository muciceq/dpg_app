
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
from view import mainWindowView, dialogAuthor, dialogPost
from model import AuthorModel, PostsModel

class MainWindow(QtWidgets.QMainWindow, mainWindowView.Ui_MainWindow):
   
    def __init__(self, parentApp, parent=None):
        super(MainWindow, self).__init__(parent)
        self.parentApp : QtWidgets.QApplication
        self.parentApp = parentApp
        self.setupUi(self)
        self.customSetup()

    def customSetup(self):
        #-------------------------menu
        self.actionInfo.triggered.connect(self.showInfo)
        #--------------------------btns
        self.btn_addAuthor.clicked.connect(self.openDialogAddAuthor)
        self.btn_deleteAuthor.clicked.connect(self.deleteAuthor)
        self.btn_addPost.clicked.connect(self.openDialogAddPost)
        self.btn_deletePost.clicked.connect(self.deletePost)
        #------------------------tableViews
        self.tableViewAuthors.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.tableViewPosts.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        #----------------------Filter & Sorting
        self.tableViewAuthors.setSortingEnabled(True)
        self.tableViewPosts.setSortingEnabled(True)
        self.spinBox1.valueChanged.connect(self.spinBox1Changing)
        self.lineEdit1.textChanged.connect(self.lineInput1Changing)
        self.spinBox2.valueChanged.connect(self.spinBox2Changing)
        self.lineEdit2.textChanged.connect(self.lineInput2Changing)

        #-------------------------------TABLEVIEW1----------------------------------
        self.model = AuthorModel.AuthorModel()
        self.model.connectionError.connect(self.showNetworkError)
        self.model.refreshData() #because of error signals..
        #sorting
        self.proxyModel = QtCore.QSortFilterProxyModel()
        self.proxyModel.setSourceModel(self.model)
        self.tableViewAuthors.setModel(self.proxyModel)
        #-----------
        #self.tableViewAuthors.setModel(self.model)

        #--------------------------------TABLEVIEW2----------------------------------
        self.model2 = PostsModel.PostsModel()
        self.model2.connectionError.connect(self.showNetworkError)
        self.model2.refreshData()
        #sorting
        self.proxyModel2 = QtCore.QSortFilterProxyModel()
        self.proxyModel2.setSourceModel(self.model2)
        self.tableViewPosts.setModel(self.proxyModel2)

    def openDialogAddAuthor(self):
        def addAuthor(firstName, lastName):
            self.model.insertRows(0, ["GET_FROM_DB",firstName,lastName])

        #self.data.append([1,5,5])
        #self.tableViewAuthors.proxyModel().layoutChanged.emit()
        dialogWindow = dialogAuthor.Ui_Dialog()
        dialogWindow.dialogCancelSignal.connect(self.closeWindow)
        dialogWindow.dialogSuccesSignal.connect(lambda firstName, lastName: addAuthor(firstName, lastName))
        self.addWindow(dialogWindow)

    def deleteAuthor(self):
        index = self.tableViewAuthors.currentIndex()
        row = index.row()
        self.model.removeRows(row)
        self.model2.refreshData()

    def openDialogAddPost(self):
        def addPost(fk, title, text):
            self.model2.insertRows(0, ["GET ID FROM DB",fk,title,text])

        set_ids = set()
        for item in self.model._data:
            set_ids.add(str(item[0]))
        dialogWindow = dialogPost.Ui_Dialog(set_ids)
        dialogWindow.dialogCancelSignal.connect(self.closeWindow)
        dialogWindow.dialogSuccesSignal.connect(lambda fk, title ,text: addPost(fk, title, text))
        self.addWindow(dialogWindow)

    def deletePost(self):
        index = self.tableViewPosts.currentIndex()
        row = index.row()
        self.model2.removeRows(row)

    def spinBox1Changing(self):
        self.proxyModel.setFilterKeyColumn(self.spinBox1.value())

    def lineInput1Changing(self):
        self.proxyModel.setFilterFixedString(self.lineEdit1.text())
    
    def spinBox2Changing(self):
        self.proxyModel2.setFilterKeyColumn(self.spinBox2.value())

    def lineInput2Changing(self):
        self.proxyModel2.setFilterFixedString(self.lineEdit2.text())

    def showInfo(self):
        QMessageBox.warning(self, "Important!", "Brought to you by Glos & Snětivý")


    def showNetworkError(self, e):
        text = str(e.args)
        QMessageBox.critical(self, "Error", text)

    def addWindow(self, dialogWindow):
        dialogWindow.show()
        self.parentApp.windows.append(dialogWindow)
        dialogWindow.setWindowFlags(dialogWindow.windowFlags())

    def closeWindow(self):
        if(len(self.parentApp.windows)-1!=0): #Always keep the main frame inside the array.
            self.parentApp.windows.pop(len(self.parentApp.windows)-1)#App no longer knows about this window -> GC

