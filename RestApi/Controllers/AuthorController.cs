﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestApi.Data;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")] // url: api/author
    public class AuthorController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public AuthorController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/author
        [HttpGet]
        public async Task<IActionResult> GetAuthors()
        {
            var authors = await _context.Authors.ToListAsync();
            return Ok(authors);
        }

        // POST: api/author
        [HttpPost]
        public async Task<IActionResult> Post(Author data)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _context.Authors.AddAsync(data);
                    await _context.SaveChangesAsync();

                    return CreatedAtAction("GetAuthor", new { id = data.authorId }, data);
                }

                return new JsonResult("Invalid input data") { StatusCode = 400 };
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        // GET: api/author/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAuthor(int id)
        {
            var author = await _context.Authors.FirstOrDefaultAsync(x => x.authorId == id);

            if (author == null) return NotFound();

            return Ok(author);
        }

        // PUT: api/author/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, [FromBody] Author data)
        {
            if (id != data.authorId) return BadRequest();

            var oldAuthor = await _context.Authors.FirstOrDefaultAsync(x => x.authorId == id);

            if (oldAuthor == null) return NotFound();

            oldAuthor.firstName = data.firstName;
            oldAuthor.lastName = data.lastName;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/author/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuthor(int id)
        {
            var author = await _context.Authors.FirstOrDefaultAsync(x => x.authorId == id);
            var authorPosts = await _context.Posts.Where(x => x.authorId == id).ToListAsync();

            if (author == null) return NotFound();

            foreach (Post post in authorPosts)
            {
                _context.Posts.Remove(post);
            }

            _context.Authors.Remove(author);
            await _context.SaveChangesAsync();
            
            return Ok(author);
        }


    }
}
