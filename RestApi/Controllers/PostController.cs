﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestApi.Data;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")] // url: api/post
    public class PostController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public PostController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/post
        [HttpGet]
        public async Task<IActionResult> GetPosts()
        {
            var posts = await _context.Posts.ToListAsync();
            return Ok(posts);
        }

        // POST: api/post
        [HttpPost]
        public async Task<IActionResult> Post(Post data)
        {
            if (ModelState.IsValid)
            {
                var author = await _context.Authors.FirstOrDefaultAsync(a => a.authorId == data.authorId);
                if (author == null) return NotFound("Author not Found");
                    
                await _context.Posts.AddAsync(data);

                author.Posts.Add(data);

                await _context.SaveChangesAsync();

                return CreatedAtAction("GetPost", new { id = data.postId }, data);
            }

            return new JsonResult("Invalid input data") { StatusCode = 400 };
        }

        // GET: api/post/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPost(int id)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(x => x.postId == id);

            if (post == null) return NotFound();

            return Ok(post);
        }

        // GET: api/post/author/1
        [HttpGet("author/{authorId}")]
        public async Task<IActionResult> GetPostByAuthorId(int authorId)
        {
            var author = await _context.Authors.FirstOrDefaultAsync(x => x.authorId == authorId);

            if (author == null) return NotFound("Author Not Found");

            var post = await _context.Posts.Where(x => x.authorId == authorId).ToListAsync();

            if (post.Count == 0) return NotFound("Posts Not Found");

            return Ok(post);
        }

        // PUT: api/post/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, [FromBody] Post data)
        {
            if (id != data.postId) return BadRequest();

            var oldPost = await _context.Posts.FirstOrDefaultAsync(x => x.postId == id);

            if (oldPost == null) return NotFound();

            oldPost.text = data.text;
            oldPost.title = data.title;
            oldPost.authorId = data.authorId;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE: api/author/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(x => x.postId == id);

            if (post == null) return NotFound();

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            return Ok(post);
        }
    }
}
