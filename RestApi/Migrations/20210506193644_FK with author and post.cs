﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestApi.Migrations
{
    public partial class FKwithauthorandpost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Posts_authorId",
                table: "Posts",
                column: "authorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Authors_authorId",
                table: "Posts",
                column: "authorId",
                principalTable: "Authors",
                principalColumn: "authorId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Authors_authorId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_authorId",
                table: "Posts");
        }
    }
}
