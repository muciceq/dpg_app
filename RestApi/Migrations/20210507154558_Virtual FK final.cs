﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestApi.Migrations
{
    public partial class VirtualFKfinal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Authors_authorId",
                table: "Posts");

            migrationBuilder.AlterColumn<int>(
                name: "authorId",
                table: "Posts",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Authors_authorId",
                table: "Posts",
                column: "authorId",
                principalTable: "Authors",
                principalColumn: "authorId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Authors_authorId",
                table: "Posts");

            migrationBuilder.AlterColumn<int>(
                name: "authorId",
                table: "Posts",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Authors_authorId",
                table: "Posts",
                column: "authorId",
                principalTable: "Authors",
                principalColumn: "authorId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
