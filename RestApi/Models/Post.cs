﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Models
{
    public class Post
    {
        [Key]
        public int postId { get; set; }

        public string text { get; set; }

        [Required]
        [MaxLength(100)]
        public string title { get; set; }


        [Required]
        public int authorId { get; set; }

    }
}
