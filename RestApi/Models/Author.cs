﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestApi.Models
{
    public class Author
    {
        [Key]
        public int authorId { get; set; }

        [Required]
        [MaxLength(20)]
        public string firstName { get; set; }

        [Required]
        [MaxLength(20)]
        public string lastName { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
